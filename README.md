NammIssue : This android application lets the user to report the day to day civic problems to
the concerned authorities and thus helps in improving the issue reporting process that currently
exists by specifying the exact location using Google maps API and a draggable marker.The app
also maintains the anonymity of the complaint and yet ensures that issues are reported to the
concerned people.
Language used: Java
IDE: Android Studio
